# Translation of prometheus-blackbox-exporter debconf templates to french.
# Copyright (C) 2020, French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the prometheus-blackbox-exporter package.
#
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: prometheus-blackbox-exporter\n"
"Report-Msgid-Bugs-To: prometheus-blackbox-exporter@packages.debian.org\n"
"POT-Creation-Date: 2020-07-25 19:32+0200\n"
"PO-Revision-Date: 2020-08-22 12:25+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Enable additional network privileges for ICMP probing?"
msgstr ""
"Faut-il activer des privilèges réseau supplémentaires pour le test d'ICMP ?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"/usr/bin/prometheus-blackbox-exporter requires the CAP_NET_RAW capability to "
"be able to send out crafted packets to targets for ICMP probing."
msgstr ""
"/usr/bin/prometheus-blackbox-exporter a besoin de la capacité CAP_NET_RAW "
"pour pouvoir envoyer des paquets élaborés à des cibles pour le test d'ICMP."


#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"ICMP probing will not work unless this option is enabled, or prometheus-"
"blackbox-exporter runs as root."
msgstr ""
"Le test d'ICMP ne fonctionnera pas à moins que cette option ne soit activée, "
"ou alors prometheus-blackbox-exporter ne sera exécuté qu'avec les droits du "
"superutilisateur."
