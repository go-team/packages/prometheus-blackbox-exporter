Source: prometheus-blackbox-exporter
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Filippo Giunchedi <filippo@debian.org>,
           Martina Ferrari <tina@debian.org>,
           Daniel Swarbrick <dswarbrick@debian.org>,
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-alecthomas-units-dev,
               golang-github-andybalholm-brotli-dev,
               golang-github-miekg-dns-dev,
               golang-github-prometheus-client-golang-dev (>= 1.19.0),
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev (>= 0.42.0),
               golang-github-prometheus-exporter-toolkit-dev (>= 0.13.0),
               golang-golang-x-net-dev,
               golang-google-grpc-dev (>= 1.34.0),
               golang-gopkg-alecthomas-kingpin.v2-dev,
               golang-gopkg-yaml.v2-dev,
               golang-gopkg-yaml.v3-dev,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-blackbox-exporter
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-blackbox-exporter.git
Homepage: https://github.com/prometheus/blackbox_exporter
XS-Go-Import-Path: github.com/prometheus/blackbox_exporter

Package: prometheus-blackbox-exporter
Section: net
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: adduser,
         debconf,
         libcap2-bin,
         ${misc:Depends},
         ${shlibs:Depends},
Static-Built-Using: ${misc:Static-Built-Using}
Description: blackbox prober for Prometheus
 The blackbox exporter allows blackbox probing of network endpoints over HTTP,
 HTTPS, DNS, TCP and ICMP. Additional modules can be defined to suit other
 needs.
 .
 Querying of endpoints happens via HTTP GET queries, by specifying the target
 name and what kind of probing to execute. Results from the probe are returned
 as a set of Prometheus metrics.
